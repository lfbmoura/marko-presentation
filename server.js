require('marko/node-require');

const Koa = require('koa');
const serve = require('koa-static');
const template = require('./src/layouts/index.marko');
const app = new Koa();

app.use(serve('./public'));

app.use((ctx) => {
	ctx.type = 'html';
	ctx.body = template.stream(ctx.params);
});

const port = process.env.PORT || 3456;
app.listen(port, () => console.log(`Server running on port ${port}`));
