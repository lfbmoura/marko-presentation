/* eslint global-require: 0 */
require('../src/sass/pack.sass');
require('../node_modules/highlight.js/styles/monokai.css');
const { Router } = require('marko-router5');
const routes = require('./routes');

Router.render({
	routes,
	options: {
		defaultRoute: '/',
		initialRoute: window.location.pathname,
	},
	initialPath: window.location.pathname,
	noWrapper: true,
}).then((render) => {
	render.appendTo(document.body);
});
