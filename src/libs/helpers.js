/* eslint global-require: 0 */
class Helper {
	static Anime(...args) {
		if (process.browser) {
			const anime = require('animejs/lib/anime');
			anime(...args);
		}
		return null;
	}

	static BasicComponent(Component) {
		const newComp = Component;
		const transits = {
			// int
			enterLeft: { translateX: [-100, 0], opacity: [0, 1] },
			enterRight: { translateX: [100, 0], opacity: [0, 1] },
			enterTop: { translateY: [-100, 0], opacity: [0, 1] },
			enterBottom: { translateY: [100, 0], opacity: [0, 1] },
			popIn: { scale: [0.7, 1], opacity: [0, 1] },
			fadeIn: { opacity: [0, 1], easing: 'easeOutCirc' },
			// out
			exitLeft: { translateX: [0, -100], opacity: [1, 0] },
			exitRight: { translateX: [0, 100], opacity: [1, 0] },
			exitTop: { translateY: [0, -100], opacity: [1, 0] },
			exitBottom: { translateY: [0, 100], opacity: [1, 0] },
			popOut: { scale: [1, 0.7], opacity: [1, 0] },
			fadeOut: { opacity: [1, 0], easing: 'easeOutCirc' },
		};

		newComp.prototype.animeIn = (
			transition,
			options,
			stagger,
			ev,
			node,
		) => {
			const children = [].slice.call(node.childNodes, 0).filter(c => c.style);
			// const children = node.childNodes;
			const delay = stagger ? (el, i) => i * stagger : 0;
			if (process.browser && transition && options && ev && node) {
				if (stagger) {
					children.forEach((c) => {
						c.style.opacity = 0;
					});
				} else {
					node.style.opacity = 0;
				}

				Helper.Anime({
					targets: stagger ? children : node,
					duration: 600,
					delay,
					...transits[transition || 'fadeIn'],
					...options,
				});
			}
		};
		newComp.prototype.animeOut = (
			transition,
			options,
			stagger,
			ev,
			node,
		) => {
			ev.preventDefault();
			const children = [].slice.call(node.childNodes, 0).filter(c => c.style);
			// const children = node.childNodes;
			const delay = stagger ? (el, i) => i * stagger : 0;
			if (process.browser && transition && options && ev && node) {
				Helper.Anime({
					targets: stagger ? children : node,
					duration: 600,
					delay,
					complete: () => { ev.detach(); },
					...transits[transition || 'fadeOut'],
					...options,
				});
			}
		};
		return newComp;
	}
}

module.exports = Helper;
