const { navigate } = require('marko-router5');
const { findIndex } = require('lodash');
const { BasicComponent } = require('./libs/helpers');
const slides = require('./slides');

class App {
  async onCreate(input) {
    this.state = {
      mobile: false,
			ready: false,
			slides,
			currentSlide: 0,
    };
  }

  onMount() {
		const index = findIndex(slides, s => s.route === window.location.pathname);
		this.setState({ currentSlide: index });
    this.subscribeTo(window).on('keydown', (ev) => {
      if (ev.key === 'ArrowRight') this.next();
      if (ev.key === 'ArrowLeft') this.previous();
    });
	}

	next() {
		const lastSlide = this.state.currentSlide < this.state.slides.length - 1;
		if (lastSlide) {
			this.setState({ currentSlide: this.state.currentSlide += 1 });
			this.nav(this.state.slides[this.state.currentSlide].route);
		}
	}

	previous() {
		const firstSlide = this.state.currentSlide > 0;
		if (firstSlide) {
			this.setState({ currentSlide: this.state.currentSlide -= 1 });
			this.nav(this.state.slides[this.state.currentSlide].route);
		}
	}

	nav(route) {
		navigate(`${route}`);
	}
}

module.exports = BasicComponent(App);
