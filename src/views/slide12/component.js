const hljs = require('highlight.js');
const { BasicComponent, Anime } = require('../../libs/helpers');

class Slide {
	onMount() {
		hljs.highlightBlock(this.getEl('code1'));
	}
}

module.exports = BasicComponent(Slide);
