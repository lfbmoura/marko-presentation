module.exports = `// Compiled using marko@4.18.16 - DO NOT EDIT
"use strict";

var marko_template = module.exports = require("marko/dist/vdom").t(__filename),
    components_helpers = require("marko/dist/runtime/components/helpers"),
    marko_registerComponent = components_helpers.rc,
    marko_componentType = marko_registerComponent("/marko-color-picker$0.0.3/index.marko", function() {
      return module.exports;
    }),
    marko_renderer = components_helpers.r,
    marko_defineComponent = components_helpers.c,
    marko_helpers = require("marko/dist/runtime/vdom/helpers"),
    marko_loadTag = marko_helpers.t,
    component_globals_tag = marko_loadTag(require("marko/dist/core-tags/components/component-globals-tag")),
    color_picker_template = require("/marko-color-picker/components/color-picker/index.marko"),
    color_picker_tag = marko_loadTag(color_picker_template),
    init_components_tag = marko_loadTag(require("marko/dist/core-tags/components/init-components-tag")),
    await_reorderer_tag = marko_loadTag(require("marko/dist/core-tags/core/await/reorderer-renderer")),
    marko_createElement = marko_helpers.e,
    marko_const = marko_helpers.const,
    marko_const_nextId = marko_const("841697"),
    marko_node0 = marko_createElement("head", null, "1", null, 1, 0, {
        i: marko_const_nextId()
      })
      .e("title", null, null, null, 1)
        .t("Welcome | Marko Demo"),
    marko_node1 = marko_createElement("h1", null, "4", null, 1, 0, {
        i: marko_const_nextId()
      })
      .t("Welcome to Marko!");

function render(input, out, __component, component, state) {
  var data = input;

  out.be("html", null, "0", component);

  out.n(marko_node0, component);

  out.be("body", null, "3", component);

  component_globals_tag({}, out);

  out.n(marko_node1, component);

  color_picker_tag({
      colors: [
          "#333745",
          "#E63462",
          "#FE5F55",
          "#C7EFCF",
          "#EEF5DB",
          "#00B4A6",
          "#007DB6",
          "#FFE972",
          "#9C7671",
          "#0C192B"
        ]
    }, out, __component, "5");

  init_components_tag({}, out);

  await_reorderer_tag({}, out, __component, "6");

  out.ee();

  out.ee();
}

marko_template._ = marko_renderer(render, {
    e_: true,
    f_: marko_componentType
  });

marko_template.Component = marko_defineComponent({}, marko_template._);

marko_template.meta = {
    id: "/marko-color-picker$0.0.3/index.marko",
    tags: [
      "marko/dist/core-tags/components/component-globals-tag",
      "/marko-color-picker/components/color-picker/index.marko",
      "marko/dist/core-tags/components/init-components-tag",
      "marko/dist/core-tags/core/await/reorderer-renderer"
    ]
  };
`;
