module.exports = `class Counter extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			count: 0
		}

		function doIncrement(delta) {
			this.setState(function(prevState, props) {
				return {
					count: prevState.count + delta
				};
			});
		}
		this.decrement = doIncrement.bind(this, -1);
		this.increment = doIncrement.bind(this, 1);
	}
	render() {
		var decrement = this.decrement;
		var increment = this.increment;

		var count = this.state.count;

		var countClassName = 'count';
		if (count > 0) {
			countClassName += ' positive';
		} else if (count < 0) {
			countClassName += ' negative';
		}

		return (
			<div className="click-count">
				<div className={countClassName}>
					{count}
				</div>
				<button onClick={decrement}>
					-1
				</button>
				<button onClick={increment}>
					+1
				</button>
			</div>
		);
	}
}

ReactDOM.render(
	<Counter/>,
	document.getElementById('root')
);
`;
