const hljs = require('highlight.js');
const { BasicComponent, Anime } = require('../../libs/helpers');

class Slide {
	onMount() {
		hljs.highlightBlock(this.getEl('code1'));
		hljs.highlightBlock(this.getEl('code2'));

		const code = this.getEl('code1').getBoundingClientRect().height;
		const wrapper = this.getEl('codescroll').getBoundingClientRect().height;

		Anime({
			targets: this.getEl('codescroll'),
			scrollTop: code - wrapper,
			loop: true,
			easing: 'linear',
			direction: 'alternate',
			delay: 1000,
			duration: 4000,
		});
	}
}

module.exports = BasicComponent(Slide);
