module.exports = `// Compiled using marko@4.12.4 - DO NOT EDIT
"use strict";

var marko_template = module.exports = require("marko/dist/html").t(__filename),
    marko_componentType = "/webapp$1.8.2-c/plugins/details-agency-info/index.marko",
    marko_component = require("./component"),
    components_helpers = require("marko/dist/components/helpers"),
    marko_renderer = components_helpers.r,
    marko_defineComponent = components_helpers.c,
    module_marko_module = require("../../helpers/marko.js"),
    marko_module = module_marko_module.default || module_marko_module,
    t = module_marko_module.t,
    marko_helpers = require("marko/dist/runtime/html/helpers"),
    marko_escapeXml = marko_helpers.x,
    marko_attr = marko_helpers.a,
    marko_escapeXmlAttr = marko_helpers.xa;

function render(input, out, __component, component, state) {
  var data = input;

  out.w("<div class=\"broker-card agent-advertiser bg-white text-xs-center text-md-left\"><h3>" +
    marko_escapeXml(t("AGENT_ADVERTISER")) +
    "</h3><div class=\"broker-box\"><div class=\"media\"><div class=\"media-left\"><img" +
    marko_attr("src", state.listing_owner_logo) +
    " width=\"120\" height=\"60\"" +
    marko_attr("alt", state.listing_owner_name) +
    "></div><div class=\"media-body\"><h4>" +
    marko_escapeXml(state.listing_owner_name) +
    "</h4><a href=\"" +
    marko_escapeXmlAttr("tel:" + state.listing_owner_phone) +
    "\" target=\"_blank\">" +
    marko_escapeXml(state.listing_owner_phone) +
    "</a></div></div></div></div>");
}

marko_template._ = marko_renderer(render, {
    _l_: marko_componentType
  }, marko_component);

marko_template.Component = marko_defineComponent(marko_component, marko_template._);

marko_template.meta = {
    id: "/webapp$1.8.2-c/plugins/details-agency-info/index.marko",
    component: "./"
  };
`;
