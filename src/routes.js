/* eslint global-require: 0 */
module.exports = [
  {
    name: 'home',
    path: '/',
    component: require('./index.marko'),
    children: [
			{ name: 'slide1', path: 'slide1', component: require('./views/slide1/index.marko')},
			{ name: 'slide2', path: 'slide2', component: require('./views/slide2/index.marko'), children: [
				{ name: 'step', path: ':step', component: require('./views/slide2/index.marko')},
			]},
			{ name: 'slide3', path: 'slide3', component: require('./views/slide3/index.marko')},
			{ name: 'slide4', path: 'slide4', component: require('./views/slide4/index.marko')},
			{ name: 'case', path: 'case', component: require('./views/case/index.marko'), children: [
				{ name: 'slide5', path: '/slide5', component: require('./views/slide5/index.marko')},
				{ name: 'slide6', path: '/slide6', component: require('./views/slide6/index.marko')},
				{ name: 'slide7', path: '/slide7', component: require('./views/slide7/index.marko')},
				{ name: 'slide8', path: '/slide8', component: require('./views/slide8/index.marko')},
				{ name: 'slide9', path: '/slide9', component: require('./views/slide9/index.marko')},
				{ name: 'slide10', path: '/slide10', component: require('./views/slide10/index.marko')},
				{ name: 'slide11', path: '/slide11', component: require('./views/slide11/index.marko')},
			] },
			{ name: 'how', path: 'how', component: require('./views/how/index.marko'), children: [
				{ name: 'slide12', path: '/slide12', component: require('./views/slide12/index.marko')},
				{ name: 'slide13', path: '/slide13', component: require('./views/slide13/index.marko')},
				{ name: 'slide14', path: '/slide14', component: require('./views/slide14/index.marko')},
				{ name: 'slide15', path: '/slide15', component: require('./views/slide15/index.marko')},
			] },
			{ name: 'about', path: 'about', component: require('./views/about/index.marko'), children: [
				{ name: 'slide16', path: '/slide16', component: require('./views/slide16/index.marko')},
				{ name: 'slide17', path: '/slide17', component: require('./views/slide17/index.marko')},
				{ name: 'slide18', path: '/slide18', component: require('./views/slide18/index.marko')},
				{ name: 'slide19', path: '/slide19', component: require('./views/slide19/index.marko')},
				// { name: 'slide20', path: '/slide20', component: require('./views/slide20/index.marko')},
			] },
		],
  },
];



